#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <artis-star/common/time/DoubleTime.hpp>

// ajout de la structure de données pour les paramètres
struct ReserveParameters
{
    double RH_init;
    double inflow;
    double outflow;
};

struct vars
{
    enum
    {
        RH
    };
};

class Reserve : public artis::pdevs::Dynamics<artis::common::DoubleTime, Reserve, ReserveParameters>
{
public:
    Reserve(const std ::string &name,
            const artis::pdevs::Context<artis::common::DoubleTime, Reserve, ReserveParameters> &context)
        : artis::pdevs::Dynamics<artis::common::DoubleTime, Reserve, ReserveParameters>(name, context),
          _RH_init(context.parameters().RH_init),
          _inflow(context.parameters().inflow),
          _outflow(context.parameters().outflow)
    {
        observables({{vars::RH, "RH"}});
    }
    ~Reserve() override = default;

    void start(const artis::common::DoubleTime::type &t) override
    {
        _phase = Phase ::INIT;
        _RH = _RH_init;
    }

    void dint(const artis::common::DoubleTime::type &time) override
    {
        if (_phase == Phase ::INIT)
        {
            _phase = Phase ::IDLE;
        }
        else
        {
            double value = _RH + _inflow - _outflow;
            if (_RH < 0)
            {
                _RH = 0;
            }
            else
            {
                _RH = value;
            }
        }
    }

    artis::common::DoubleTime::type ta(
        const artis::common::DoubleTime::type &t) const override
    {
        if (_phase == Phase::INIT)
            return 0;
        if (_phase == Phase::IDLE)
            return 1;
        return 0;
    }

    artis::common::Value observe(const artis::common::DoubleTime::type &t,
                                 unsigned int index) const
    {
        if (index == vars ::RH)
            return (double)_RH;
        // Si le modèle est observé sur un identifiant inconnu , nous
        // ne fournissons aucune information
        return artis::common::Value();
    }

private:
    struct Phase
    {
        enum values
        {
            INIT,
            IDLE
        };
    };
    Phase ::values _phase;
    double _RH;
    double _RH_init;
    double _inflow;
    double _outflow;
};
