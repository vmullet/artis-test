#include <iostream>
#include <artis-star/common/context/Context.hpp>
#include <artis-star/common/RootCoordinator.hpp>
#include <artis-star/kernel/pdevs/Coordinator.hpp>
#include "graph_manager.hpp"

int main()
{
    std::cout << "Init\n";
    ReserveParameters parameters = {100, 7, 10};
    artis::common::context::Context<artis::common::DoubleTime> context(0, 10);
    artis::common::RootCoordinator<
        artis::common::DoubleTime, artis::pdevs::Coordinator<artis::common::DoubleTime,
                                                             OnlyOneGraphManager, ReserveParameters>>
        rc(context, "root", parameters, artis::common::NoParameters());
    rc.switch_to_timed_observer(0.1);
    rc.run(context);
    return 0;
}
