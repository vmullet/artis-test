#!/bin/bash


if [ -d "./build" ] 
then
    rm -rf build
fi


mkdir build

cd build

echo "===============CMAKE===================="
cmake -DCMAKE_INSTALL_PREFIX=$HOME/usr ..

echo "===============MAKE====================="
make

echo "==============RUN=================="
./artis-test