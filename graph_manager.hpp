#include <artis-star/kernel/pdevs/GraphManager.hpp>
#include <artis-star/common/Parameters.hpp>
#include "models.hpp"

class OnlyOneGraphManager : public artis::pdevs::GraphManager<artis::common::DoubleTime, ReserveParameters>
{
public:
    struct submodels
    {
        enum
        {
            OneM
        };
    };
    OnlyOneGraphManager(artis::common::Coordinator<artis::common::DoubleTime> *coordinator,
                        const ReserveParameters &parameters,
                        const artis::common::NoParameters &graph_parameters)
        : artis::pdevs::GraphManager<artis::common::DoubleTime, ReserveParameters>(coordinator, parameters,
                                                                                   graph_parameters),
          model("m", parameters)
    {
        add_child(submodels ::OneM, &model);
    }

    ~OnlyOneGraphManager() override = default;

private:
    artis::pdevs::Simulator<artis::common::DoubleTime, Reserve, ReserveParameters> model;
};
